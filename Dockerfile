FROM debian as builder

RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y -q --no-install-recommends \
  ca-certificates \
  cmake  \
  curl   \
  git    \
  lbzip2 \
  make   \
  sudo   \
  wget   \
  xz-utils && \
  rm -rf /var/lib/apt/lists/*

RUN addgroup vita && \
    adduser  --disabled-password --ingroup vita --home /home/vita vita

WORKDIR /tmp

RUN git clone https://github.com/vitasdk/vdpm && \
    cd vdpm && \
    ./bootstrap-vitasdk.sh && \
    export VITASDK=/usr/local/vitasdk && \
    export PATH=$VITASDK/bin:$PATH && \
    ./install-all.sh && \
    cd /tmp && \
    rm -rf vdpm

ENV VITASDK /usr/local/vitasdk
ENV PATH    $VITASDK/bin:$PATH

USER vita

WORKDIR /home/vita
