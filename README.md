# Docker container for Vita SDK

## Build

    make

## Run

    docker run --rm -ti -v $(pwd)/<your code directory>:/home/vita/<your code directory> local/docker.vitasdk bash
